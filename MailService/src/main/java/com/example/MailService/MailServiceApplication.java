package com.example.MailService;

import com.example.MailService.controller.MailController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class MailServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailServiceApplication.class, args);
//		TimerTask timerTask = new TimerTask() {
//			@Override
//			public void run() {
//				System.out.println("task  run:"+ new Date());
//			}
//		};
//		Timer timer = new Timer();
//		timer.schedule(timerTask,10 ,3000);

	}

}
