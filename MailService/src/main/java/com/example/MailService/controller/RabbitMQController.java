package com.example.MailService.controller;


import com.example.MailService.model.MailRequest;
import com.example.MailService.model.TestEntity;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rabbit")
public class RabbitMQController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping(value = "/send")
    public String sendRabbitMQ(@RequestParam("exchangeName") String exchange, @RequestParam("routingKey") String routingKey,
                           @RequestParam("messageData") String messageData) {
        rabbitTemplate.convertAndSend(exchange, routingKey, messageData);
//        MailRequest mail = new MailRequest() ;
//        mail.setMailContent("aaaaaaaaaaaa");
//        mail.setMailTo("18020749@vnu.edu.vn") ;
//        mail.setMailFrom("doxuanlam0902@gmail.com") ;
//        mail.setMailSubject("bbbbbbb");
//        amqpTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_NAME, RabbitMQConfig.ROUTING_KEY, mail);

        return "Message sent to the RabbitMq Successfully";
    }
    @PostMapping(value = "/send1")
    public String sendRabbit(@PathVariable(name = "message") String message) {

        MailRequest mail = new MailRequest() ;
        mail.setMailContent("aaaaaaaaaaaa");
        mail.setMailTo("18020749@vnu.edu.vn") ;
        mail.setMailFrom("doxuanlam0902@gmail.com") ;
        mail.setMailSubject("bbbbbbb");
//        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE, RabbitMQConfig.ROUTING_KEY, mail);

        return "Message sent Successfully";
    }


}