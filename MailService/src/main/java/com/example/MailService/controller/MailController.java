package com.example.MailService.controller;

import com.example.MailService.Consume.ConsumerConfig;
import com.example.MailService.model.Mail;
import com.example.MailService.model.MailRequest;
import com.example.MailService.service.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@RestController
@Slf4j
@RequestMapping("/api/v1")
@CrossOrigin
public class MailController {

    @Autowired
    JavaMailSender javaMailSender ;

    @Autowired
    MailService mailService ;

    @Autowired
    private AmqpTemplate amqpTemplate;

    public ConsumerConfig consumerConfig = new ConsumerConfig();

    @PostMapping(value = "/sendMail")
    public String sendMail(@RequestBody MailRequest mail) throws IOException, TimeoutException {

        // Create a Simple MailMessage.
        String text = consumerConfig.getMessage() ;
        System.out.println("MESSAGE"  + text );
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(mail.getMailTo());
        message.setSubject("Test Simple Email");
        message.setText(text);

        // Send Message!
        javaMailSender.send(message);


        return "Email Sent!";

//        Message message =  amqpTemplate.receive("userQueue") ;
////        String  mail1 = amqpTemplate.convertSendAndReceive("userQueue").toString() ;
////        System.out.println(mail1);
//        if(message != null) {
//            System.out.println(message.getBody());
//            mailService.sendEmail(mail);
//            return "Send Mail Complete !!!" ;
//        }
//        return "Send Mail Failed" ;


    }

}
